// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const getYearsOfAllCars = require('./problem4.js');
function findNoOfOlderCars(inventory,yearGivenByUser){
    if(Array.isArray(inventory) && typeof yearGivenByUser == 'number') {
        let AllCaryears = getYearsOfAllCars(inventory);
        let olderCars = AllCaryears.filter((carYear) => {
            return carYear<yearGivenByUser;
        });
        if(olderCars.length==0) {
            throw new Error(`There no cars older than ${yearGivenByUser}`);
        }
        return olderCars
    } else {
        throw new Error('Arguments passed to the function findNoOfOlderCars is/are not valid type.')
    }
}

module.exports = findNoOfOlderCars;