// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function getCarsByBrand(inventory,brand1,brand2) {
    if(Array.isArray(inventory) && typeof brand1 == 'string' && typeof brand2 == 'string'){
        let carByBrand = inventory.filter((car) => {
            return car.car_make.toLowerCase().includes(brand1.toLowerCase()) || 
            car.car_make.toLowerCase().includes(brand2.toLowerCase());
        })
        if(carByBrand.length==0) {
            throw new Error(`no ${brand1} ${brand2} cars found.`)
        } 
        let updatedCarsByBrand =  carByBrand.map((car) => {
            return JSON.stringify(car);
        });
        return updatedCarsByBrand;
    } else {
        throw new Error('Arguments passed to the function getCarsByBrand is/are not valid type.')
    }
}

module.exports = getCarsByBrand;