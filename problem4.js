// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getYearsOfAllCars (inventory) {
    if(Array.isArray(inventory)) {
        let carYears = inventory.map((car) => {
            return car.car_year;
        });
        return carYears;
    } else {
        throw new Error('Arguments passed to the function getYearsOfAllCars is not valid type.')
    }
}

module.exports = getYearsOfAllCars;