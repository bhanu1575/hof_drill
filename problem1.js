// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

function findCarById(inventory,id){
    if(Array.isArray(inventory) && typeof id == 'number') {
        let carWithGivenId = inventory.find((car) => {
           return car.id == id;
        });
        if(carWithGivenId.length==0){
            throw new Error(`no car with id ${id} found`)
        }
        return carWithGivenId;
    } else {
        throw new Error('arguments passed to the function findCarsById is/are not valid type.')
    }
}

module.exports = findCarById;