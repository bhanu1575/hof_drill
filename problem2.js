// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:

function findLastCar(inventory){
    if(Array.isArray(inventory)) {
        let lastCar = inventory.reduce((accumulator,currentValue) => {
            return currentValue;
        })
        return lastCar;
    } else {
        throw new Error('Arguments passed into the function findLastCar is not valid type');
    }
}

module.exports = findLastCar;