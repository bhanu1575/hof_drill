// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function getAllCarModels (inventory) {
    if(Array.isArray(inventory)) {
        let AllCarModels = inventory.map((car) => {
            return car.car_model;
        });
        return AllCarModels.sort(); 
    } else {
        throw new Error('Arguments passed to the function getAllCarModels is not valid type.');
    }
}

module.exports = getAllCarModels;