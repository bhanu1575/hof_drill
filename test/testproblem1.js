// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

let inventory = require('../data.js');
const findCarById = require('../problem1.js');
let id = 33;
try {
    let car = findCarById(inventory,id);
    // console.log(car);
    console.log(`car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`)

} catch (error) {
    console.error(error.message);
}