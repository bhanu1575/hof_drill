let inventory = require('../data.js');
const getCarsByBrand = require('../problem6.js');

let brand1 = 'BMW';
let brand2 = "Audi";

try {
    let carsByBrand=getCarsByBrand(inventory,brand1,brand2);
    console.log(carsByBrand);
} catch (error) {
    console.error(error.message);
}