// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

let inventory = require('../data.js');
let findNoOfOlderCars = require('../problem5.js');

let year = 2000;

try {
    let olderCars = findNoOfOlderCars(inventory,year);
    console.log(olderCars);
    console.log(`there are ${olderCars.length} cars older than ${year}`);
} catch (error) {
    console.error(error.message);
}