// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

let inventory = require('../data.js');
let getYearsOfAllCars = require('../problem4.js');

try {
    let carYears = getYearsOfAllCars(inventory);
    console.log(carYears);
} catch (error) {
    console.error(error.message);
}