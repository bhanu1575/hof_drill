// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:

let inventory = require('../data.js');
const findLastCar = require('../problem2.js');

try {
    let lastCar = findLastCar(inventory);
    console.log(`last car is a ${lastCar.car_make} ${lastCar.car_model}`);
} catch (error) {
    console.error(error.message);
}