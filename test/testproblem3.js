// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

let inventory = require('../data.js');
const getAllCarModels = require('../problem3.js');

try {
    let AllCarModels = getAllCarModels(inventory);
    console.log(AllCarModels);
} catch (error) {
    console.error(error.message);
}